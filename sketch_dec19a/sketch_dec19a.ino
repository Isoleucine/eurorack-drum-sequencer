#include < avr/interrupt.h >
#include "LedControl.h" 
LedControl lc=LedControl(12,11,10,1);
int rotaryInterrupt = 2; //pin to trigger rotary encoder check
int rotaryCheck = 1; //pin which is checked
int clockIn = 3; //pin for external clock
bool rotencodAxis = '0'; //sets axis to use in currentLocation, 0 is x, 1 is y
int channels [] = [4,5,6,7]; //4 outputs used
bool editMode = true; //enables editing pins
int step = 0; //current step to output
int ledState = LOW; //blink base state
unsigned lomg milliseconds = 0; //millisecond counter
int spdtHigh = 8; //pin for setting a drum step to play
int spdtLow = 9; //pin for setting a drum step to not do that
int rotencodButton = 0; //rotary encoder button used to set axis
int editButton = 13; //button used to toggle edit mode
int currentLocation [] = [0, 0]; //where the blinking and drum toggling will occur
bool pinMatrix[4][8] = {
 
  {false,  false,  false, false,  false,  false, false, false },

  {false,  false,  false, false,  false,  false, false, false },

  {false,  false,  false, false,  false,  false, false, false },

  {false,  false,  false, false,  false,  false, false, false }
 ///used to keep the drum loop
 
 
};
void setup() {
pinMode(clockIn, INPUT_PULLUP); //set to pullup to check if the signal rises 
pinMode(rotaryInterrupt, INPUT_PULLUP); //set to pullup to check if the signal rises 
pinMode(rotaryCheck, INPUT);
pinMode(rotencodButton, INPUT);
pinMode(editButton, INPUT);
pinMode(spdtHigh, INPUT); 
pinMode(spdtLow, INPUT); 
attachInterrupt(digitalPinToInterrupt(clockIn), clockIterate, RISING); //sets the clock moving the pattern ahead one
attachInterrupt(digitalPinToInterrupt(rotaryInterrupt), rotaryEvaluate, RISING); //interprets the movement of the rotary encoder
  for (int thisPin = 0; thisPin < pinCount; thisPin++) {
    pinMode(channels[thisPin], OUTPUT);
  } //sets each channel to an output
lc.shutdown(0,false); //idk what it does but it makes led driver work
lc.setIntensity(0,8); //how bright the leds will be
}

void loop() {
if (editButton == HIGH);{
  editMode = !editMode;
} //toggles the edit mode when button is hit
if (rotencodButton == HIGH);{
  RotencodAxis = !rotencodAxis;
} //same, but for the axis the rotary encocder controls

        for(int k = 0; k < 8; i++){
               if (k = step;){
               lc.setLed(0,step,0,true);
               }
               else {
                lc.setLed(0,k, false);
               } //sets the light to be high or in the matrix based on whether it's the active step in the sequence 
        }
  for(int j = 0; j <4 ; i++){
    if (pinMatrix [j][k] = true){
    lc.setLed(0,j,k,true);

  }
  else {
      lc.setLed(0,j,k,false);
  }
  }//sets each light based on its value in pinmatrix

if (editmode = true) {
   unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;

    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;)
    }
  }
lc.setled(0, currentLocation[0], currentLocation[1], ledState);
 if (spdtHigh = HIGH) {
   pinmatrix[currentLocation] = HIGH;
 }
  if (spdtLow = HIGH) {
   pinmatrix[currentLocation] = LOW;
 }  
}//if edit mode is on, blink the led that corresponds to the step to be edited
}

void clockIterate (){
  if (step == 7){
    step = 0;
  }
  else{
    for(int i=0; i<4; i++){
    digitalWrite(channel[i],channel[i][step]);
    step = step + 1;
  }
}
}//if there's a clock pulse, move the sequence forward once
void rotaryEvaluate () {
  if (editmode == true) { 
  if (rotaryCheck == HIGH) {
     currentLocation[rotencodAxis]= currentLocation[rotencodAxis] ++;
  }
  else {
    currentLocation[rotencodAxis] --;
  }
}
}//checks whether the rotary encoder was moved and sets the position accordingly
